const express = require('express');
const app = express();
const fungsi_urut = require('./lib/urutkan')
const array_lib = require("./lib/array")
const object_lib = require("./lib/object")
const tambah_lib = require("./lib/tambah")



app.use(express.urlencoded({extended:true}))

app.get('/', (req, res) => { 
    
    var x,y,z
    x=5
    y=4
    z=x+y

    //const car = {type:"Fiat", model:"500", color:"white"};

    class Car {
        constructor(brand) {
          this.carname = brand;
        }
      }
      
      mycar = new Car("Ferari is fast");


      function myFunction(p1, p2) {
        return p1 * p2;   // The function returns the product of p1 and p2
      }

      var person = {
        firstName: "John",
        lastName: "Doe",
        age: 50,
        eyeColor: "blue"
      };
      

      var cars = ["Saab", "Volvo", "BMW"];

      var text = '{ "employees" : [' +
      '{ "firstName":"John" , "lastName":"Doe" },' +
      '{ "firstName":"Anna" , "lastName":"Smith" },' +
      '{ "firstName":"Peter" , "lastName":"Jones" } ]}';

      var obj = JSON.parse(text);

    //   function validateForm() {
    //     var x = document.forms["myForm"]["fname"].value;
    //     if (x == "") {
    //       alert("Name must be filled out");
    //       return false;
    //     }
    //   }

    //   var form = `<form name="myForm" action="index.js" onsubmit='return validateForm()' method="post">
    //   Name: <input type="text" name="fname">
    //   <input type="submit" value="Submit">
    // </form>`

    // var button=`<button onclick="this.innerHTML=Date()">The time is?</button>`

    if (obj.employees[0].firstName=="Johny") {
        //greeting = "Good day";
        //var x = 'hello  '+obj.employees[0].firstName;
            if(obj.employees[0].lastName=="Doe"){
                var x = 'hello  kondisi 2'+obj.employees[0].firstName+' '+obj.employees[0].lastName ;
            }
            
      }

      else{
        var x = 'no hello'+obj.employees[0].firstName;
      }


      function Person1(first, last, age, eye) {
        this.firstName = first;
        this.lastName = last;
        this.age = age;
        this.eyeColor = eye;
        
      }

      var orang = new Person1("Edwin","Penangsang")
      Person1.prototype.nationality= "Indonesia";
      
    //   var cars = ["BMW", "Volvo", "Saab", "Ford", "Fiat", "Audi"];
      var text = "";
      //var i=1;
      for (i = 1; i < 10; i++) {
        text += i + "<br>";
      }
    
    
    return res.send(text)

})

app.post('/',(req, res)=>{

    let email = req.body.email

    return res.send(`The email value is ${email}`)
})


app.post('/tambah',(req, res)=>{
    let x =req.body.x
    let y = req.body.y

    
    
    var total =parseInt(x) + parseInt(y);

    //var summed = sum( total );

    return res.send(`The  value is ${total}`)
})
app.post('/kurang',(req, res)=>{
    let x =req.body.x
    let y = req.body.y

    
    
    var total =parseInt(x) - parseInt(y);

    //var summed = sum( total );

    return res.send(`The  value is ${total}`)
})

app.get('/kurang',(req, res)=>{
    let x =req.params.x
    let y = req.params.y

    
    
    var total =parseInt(x) - parseInt(y);

    //var summed = sum( total );

    return res.send(`The  value is ${total}`)
})


app.get('/example-1', (req, res) => {
  let data = ["Red", "Blue", "Yellow", "Green"]
  let manipulation = array_lib(data)

  return res.send(manipulation)
})

app.get('/example-2', (req, res) => {
  let data = {
      firstname: "John",
      lastname: "Doe",
      email: "john.doe@mail.com",
      gender: "men"
  }
  let result = object_lib.manipulate(data)

  return res.send(result)
})

app.get("/example-3", (req, res) => {
  let input = ["Black"]
  let data = ["Red", "Blue", "Yellow", "Green"]
  console.log("before ", data)

  let result = object_lib.merge(data = data, input = input)
  console.log("after ", result)

  return res.send(result)
})

app.get("/example-4", (req, res) => {
  let empty_data = []
  console.log("before ", empty_data)

  let color = "Red"
  object_lib.add(empty_data, color)
  console.log("after ", empty_data)

  return res.send(empty_data)
})

app.get("/tugasurutkan", (req, res) => {
  
  let data = ["zed", "Alue", "Aellow", "areen"]

  let result =fungsi_urut(data)

  return res.send(result)
})

app.get("/tugasterbalik", (req, res) => {
  
  let data = ["A", "B", "C", "D"]
  let in_data = "E"

  let result =fungsi_urut.urutkan_coy(data,in_data)

  return res.send(result)
})


app.get("/tambah_tugas", (req, res) => {

  // let z = ""
  // let a = tambah_lib(x)
  // let b = tambah_lib(y)
  // let zx = tambah_lib(z)
  var form_jumlah=`<form action="" method="POST">
          <label>Nilai 1</label><br/>
          <input type="text" name="a"><br/>
          <label>Nilai 2</label><br/>
          <input type="text" name="b"><br/>
          <input type="submit" value="simpan">
  </form>`
  let result =tambah_lib(1,20)

  return res.send(`${result}`)
})


app.get("/task1", (req, res) => {
  let colors = ["Red", "Blue", "Yellow", "Green"]
  console.log("Before ", colors)

  let result = fungsi_urut.ascending_sort(colors)
  console.log("After ", result)

  return res.send(result)
})

app.get("/task2", (req, res) => {
  let colors = ["Red", "Blue", "Yellow", "Green"]
  console.log("Before ", colors)

  let result = fungsi_urut.descending_sort(colors)
  console.log("After ", result)

  return res.send(result)
})





app.listen(3000,() => {
    console.log('program is running port 3000')
})


